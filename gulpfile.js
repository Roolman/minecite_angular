const gulp = require("gulp")
const browserSync = require("browser-sync").create()
const watch = require("gulp-watch")
const concat = require("gulp-concat")
const minify = require("gulp-minify")
const uglify = require("gulp-uglify-es").default
//const ngAnnotate = require("gulp-ng-annotate")
const gulpBabel = require("gulp-babel")
const historyApiFallback = require("connect-history-api-fallback")
const gulpCopy = require("gulp-copy")

//start server
gulp.task("server", function () {
  browserSync.init({
    server: {
      baseDir: "./src/",
      middleware: [historyApiFallback()],
      proxy: "localhost:7070/",
    },
    browser: "chrome",
    cors: true,
    port: 7070,
    notify: false,
    //host: "localhost:7070"
  })
})

//watch changes in file and reload browser
gulp.task("watch", function () {
  watch(
    // Следить за файлами и просто обновлять страницу при изменении
    [
      "./src/*.js",
      "./src/styles/*.css",
      "./src/styles/**/*.css",
      "./src/views/*.html",
      "./src/*.html",
    ],
    gulp.parallel(browserSync.reload)
  )
  watch(
    // Следить за файлами и после обновления вызвать минификацию
    ["./src/scripts/**/*.js", "./src/scripts/*.js"],
    function () {
      setTimeout(gulp.parallel("minify"), 1000)
    }
  )
})

//concat and minify my js files
gulp.task("minify", function (callback) {
  gulp
    .src([
      "./src/scripts/projconfig.js",
      "./src/scripts/JOURNALS_CAT.js",
      "./src/scripts/mineciteApp.js",
      "./src/scripts/services/*.js",
      "./src/scripts/controllers/*.js",
      "./src/scripts/directives/*.js",
    ])
    .pipe(minify())
    .pipe(concat("app.js"))
    .pipe(gulpBabel())
    .pipe(uglify())
    .pipe(gulp.dest("./src/"))
  callback()
})

// start with "gulp" command
gulp.task("default", gulp.series("minify", gulp.parallel("server", "watch")))

/* --------------------- COMPILE TO DIST  */

// Скопировать все что в src в dist
const indexPath = ["./src/*.*"]
const indexDestination = "./dist/"
gulp.task("copyIndex", function (callback) {
  gulp
    .src(indexPath)
    .pipe(gulpCopy(indexDestination))
    .pipe(gulp.dest(indexDestination))
  callback()
})
// Скопировать стили
const stylesPath = ["./src/styles/*"]
const stylesDestination = "./dist/styles/"
gulp.task("copyStyles", function (callback) {
  gulp
    .src(stylesPath)
    .pipe(gulpCopy(stylesDestination))
    .pipe(gulp.dest(stylesDestination))
  callback()
})
// Скопировать вьюшки
const viewsPath = ["./src/views/*"]
const viewsDestination = "./dist/views/"
gulp.task("copyViews", function (callback) {
  gulp
    .src(viewsPath)
    .pipe(gulpCopy(viewsDestination))
    .pipe(gulp.dest(viewsDestination))
  callback()
})

//Сконкатинировать в app.js
gulp.task("concat", function (callback) {
  gulp
    .src([
      "./src/scripts/projconfig.js",
      "./src/scripts/JOURNALS_CAT.js",
      "./src/scripts/mineciteApp.js",
      "./src/scripts/services/*.js",
      "./src/scripts/controllers/*.js",
      "./src/scripts/directives/*.js",
    ])
    .pipe(minify())
    .pipe(concat("app.js"))
    .pipe(gulpBabel())
    .pipe(uglify())
    .pipe(gulp.dest("./src/"))
  callback()
})

gulp.task(
  "build",
  gulp.series("concat", "copyIndex", "copyStyles", "copyViews")
)

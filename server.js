const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");

const app = express();

app.use(cors({ origin: "*" }));

app.use(bodyParser.json());

app.use(express.static(__dirname + "/dist/"));

// Port Number
const port = process.env.PORT || 7070;

// Index Route
app.get("/", (req, res) => {
  res.send("invaild endpoint");
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "/dist/index.html"));
});

// Start Server
app.listen(port, () => {
  console.log("Server started on port " + port);
});

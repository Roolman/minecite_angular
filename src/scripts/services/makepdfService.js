mineciteApp.factory("makepdfService", function () {
  var itemsInPDF = []

  var clearItemsInPDF = () => {
    itemsInPDF = []
    dd = JSON.parse(JSON.stringify(dd_default))
    document.getElementById("pdfExport").style.display = "none"
  }

  // Поиск элемента в списке по ключу id
  var lastIndexOf = (array, id) => {
    for (var i = array.length - 1; i >= 0; i--) {
      if (array[i].id === id) return i
    }
    return -1
  }

  // Проверка наличия элемента в списке объектов на выгрузку в пдф
  var checkIteminList = function (id) {
    if (lastIndexOf(itemsInPDF, id) >= 0) {
      return true
    } else {
      return false
    }
  }

  var getSubjects = function (subjects) {
    // var subjects_titles = []
    // subjects.forEach(function (subject) {
    //   subjects_titles.push(subject.title)
    // })
    // return subjects_titles
    return subjects
  }

  var getAuthors = function (authors) {
    // var authors_names = []
    // authors.forEach(function (author) {
    //   authors_names.push(author.name)
    // })
    // return authors_names
    return authors
  }

  var checkJournal = function (article) {
    if ("journal" in article) {
      return article.journal.title
    } else {
      return "Неизвестно"
    }
  }

  var addItemToPDF = function (article, id) {
    // Если элемент уже есть в списке, то удаляем его из списка
    if (lastIndexOf(itemsInPDF, id) >= 0) {
      // Удаление из списка
      itemsInPDF.splice(lastIndexOf(itemsInPDF, id), 1)
    } else {
      // Иначе добавляем в список
      // Добавляем статью в список
      itemsInPDF.push({
        // Статья (данные о статье берем из массива articles.data)
        article: article,
        // Id во всем списке
        id: id,
      })
    }
    if (itemsInPDF.length > 0) {
      document.getElementById("pdfExport").style.display = "block"
      $("#pdfExport span.counter").text(itemsInPDF.length.toString())
    } else {
      document.getElementById("pdfExport").style.display = "none"
    }
  }

  // Данная переменная нужна для инициализации новой выгрузки пдф
  var dd_default = {
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [
          {
            fontSize: 9,
            text: [
              {
                text: "Документ сформирован системой MineCite" + "\n",
                fontSize: 12,
                bold: true,
                color: "green",
                margin: [0, 20],
              },
              {
                text: currentPage.toString(),
              },
            ],
            alignment: "center",
          },
        ],
      }
    },
    content: [
      {
        text: "MineCite",
        style: "top_header",
        alignment: "center",
      },
      {
        text: "Перечень научных публикаций \n",
        style: "top_header",
        alignment: "center",
      },
    ],
    styles: {
      top_header: {
        fontSize: 18,
        bold: true,
        alignment: "justify",
      },

      title: {
        fontSize: 14,
        bold: true,
      },
      quote: {
        italics: true,
      },
    },
  }

  var dd = {
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [
          {
            fontSize: 9,
            text: [
              {
                text: "Документ сформирован системой MineCite" + "\n",
                fontSize: 12,
                bold: true,
                color: "green",
                margin: [0, 20],
              },
              {
                text: currentPage.toString(),
              },
            ],
            alignment: "center",
          },
        ],
      }
    },
    content: [
      {
        text: "MineCite",
        style: "top_header",
        alignment: "center",
      },
      {
        text: "Перечень научных публикаций \n",
        style: "top_header",
        alignment: "center",
      },
    ],
    styles: {
      top_header: {
        fontSize: 18,
        bold: true,
        alignment: "justify",
      },

      title: {
        fontSize: 14,
        bold: true,
      },
      quote: {
        italics: true,
      },
    },
  }

  var makePDF = function () {
    var index = 1
    console.log(itemsInPDF)
    itemsInPDF.forEach(function (item) {
      dd.content.push(
        {
          text: [
            {
              text: "\n" + index.toString() + ". " + item.article.title,
              fontSize: 14,
              bold: true,
            },
          ],
        },
        {
          text: [
            // { text: "\nКатегории: ", fontSize: 12, bold: true },
            // {
            //   text: getSubjects(item.article.subjects).join(", "),
            //   fontSize: 12,
            // },

            { text: "\nГод публикации: ", fontSize: 12, bold: true },
            { text: item.article.cover_date, fontSize: 12 },

            { text: "\nЖурнал: ", fontSize: 12, bold: true },
            { text: checkJournal(item.article), fontSize: 12 },

            { text: "\nISSN: ", fontSize: 12, bold: true },
            { text: item.article.issn, fontSize: 12 },

            { text: "\nАвторы: ", fontSize: 12, bold: true },
            { text: getAuthors(item.article.authors), fontSize: 12 },

            {
              text: "\nАннотация",
              fontSize: 14,
              bold: true,
              alignment: "center",
            },
            {
              text: "\n" + item.article.abstract + "\n",
              fontSize: 12,
              alignment: "justify",
            },

            { text: "\nСтатья в Scopus: ", fontSize: 12, bold: true },
            {
              text:
                "https://www.scopus.com/inward/record.uri?partnerID=HzOxMe3b&scp=" +
                item.article.eid.substr(7) +
                "&origin=inward",
              link:
                "https://www.scopus.com/inward/record.uri?partnerID=HzOxMe3b&scp=" +
                item.article.eid.substr(7) +
                "&origin=inward",
              fontSize: 12,
            },
            { text: "\n \nСтатья в Издательстве: ", fontSize: 12, bold: true },
            {
              text: item.article.url,
              link: item.article.url,
              fontSize: 12,
            },
          ],
        }
      )
      index += 1
    })
  }
  var d = new Date()
  var defaultFileName =
    "MineCite-Report-" +
    d.getDate().toString() +
    "-" +
    (d.getMonth() + 1).toString() +
    ".pdf"

  var getPDF = function () {
    makePDF()
    pdfMake.createPdf(dd).download(defaultFileName)
    dd = JSON.parse(JSON.stringify(dd_default)) // очищаем переменную
  }

  // То, что можно попросить у данного сервиса (публично доступные методы и данные)
  return {
    clearItemsInPDF: clearItemsInPDF,
    checkIteminList: checkIteminList,
    addItemToPDF: addItemToPDF,
    getPDF: getPDF,
  }
})

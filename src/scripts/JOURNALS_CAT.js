var JOURNALS_CAT = {
  Multidisciplinary: ["Multidisciplinary"],
  "Chemical Engineering": [
    "General Chemical Engineering",
    "Chemical Health and Safety",
    "Fluid Flow and Transfer Processes",
    "Process Chemistry and Technology"
  ],
  Chemistry: ["General Chemistry", "Analytical Chemistry", "Inorganic Chemistry", "Physical and Theoretical Chemistry"],
  "Computer Science": ["General Computer Science", "Computer Science Applications"],
  "Earth and Planetary Sciences": [
    "General Earth and Planetary Sciences",
    "Earth and Planetary Sciences (miscellaneous)",
    "Computers in Earth Sciences",
    "Earth-Surface Processes",
    "Economic Geology",
    "Geochemistry and Petrology",
    "Geology",
    "Geophysics",
    "Geotechnical Engineering and Engineering Geology",
    "Oceanography",
    "Palaeontology",
    "Space and Planetary Science"
  ],
  Energy: ["General Energy", "Energy Engineering and Power Technology", "Fuel Technology", "Renewable Energy, Sustainability and the Environment"],
  Engineering: [
    "General Engineering",
    "Engineering (miscellaneous)",
    "Aerospace Engineering",
    "Civil and Structural Engineering",
    "Control and Systems Engineering",
    "Electrical and Electronic Engineering",
    "Industrial and Manufacturing Engineering",
    "Mechanical Engineering",
    "Mechanics of Materials",
    "Ocean Engineering",
    "Safety, Risk, Reliability and Quality",
    "Building and Construction",
    "Architecture "
  ],
  "Environmental Science": [
    "General Environmental Science",
    "Ecology",
    "Environmental Chemistry",
    "Environmental Engineering",
    "Health, Toxicology and Mutagenesis",
    "Management, Monitoring, Policy and Law",
    "Nature and Landscape Conservation",
    "Pollution",
    "Waste Management and Disposal",
    "Water Science and Technology"
  ],
  "Materials Science": [
    "General Materials Science",
    "Ceramics and Composites",
    "Electronic, Optical and Magnetic Materials",
    "Materials Chemistry",
    "Metals and Alloys",
    "Surfaces, Coatings and Films"
  ],
  "Physics and Astronomy": [
    "General Physics and Astronomy",
    "Physics and Astronomy (miscellaneous)",
    "Astronomy and Astrophysics",
    "Condensed Matter Physics",
    "Nuclear and High Energy Physics",
    "Atomic and Molecular Physics, and Optics",
    "Statistical and Nonlinear Physics",
    "Surfaces and Interfaces"
  ]
}

var BACKEND_CAT = [
  "General Engineering",
  "General Energy",
  "General Materials Science",
  "General Computer Science",
  "General Earth and Planetary Sciences",
  "General Chemistry",
  "General Chemical Engineering",
  "General Environmental Science"
]

mineciteApp.controller("adminCtrl", [
  "$scope",
  "$resource",
  "$timeout",
  "DTOptionsBuilder",
  "DTDefaultOptions",
  "$rootScope",
  "$location",
  "AuthenticationService",
  function ($scope, $resource, $timeout, DTOptionsBuilder, DTDefaultOptions, $rootScope, $location, AuthenticationService) {
    // Инициализация страницы с загрузкой журналов и таблицы
    $scope.InitJournals = function () {
      $scope.showTable = false

      $scope.journals = []

      $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption("processing", true)
        .withPaginationType("simple_numbers")
        .withLanguage({
          sEmptyTable: "Нет данных или загрузка...",
          sInfo: "Показано _START_ до _END_ от _TOTAL_ журналов",
          sInfoEmpty: "Показано 0 до 0 от 0 журналов",
          sInfoFiltered: "",
          sInfoPostFix: "",
          sInfoThousands: ",",
          sLengthMenu: "Показывать по _MENU_ журналов на странице",
          sLoadingRecords: "Загрузка...",
          sProcessing: "Обработка...",
          sSearch: "Поиск:",
          sZeroRecords: "Результаты не найдены",
          oPaginate: {
            sFirst: "Первая",
            sLast: "Последняя",
            sNext: "След.",
            sPrevious: "Пред."
          },
          oAria: {
            sSortAscending: ": activate to sort column ascending",
            sSortDescending: ": activate to sort column descending"
          }
        })

      $scope.journalsserverResource.get().$promise.then(
        function (response) {
          $scope.journals = response.journals
          $timeout(function () {
            $scope.showTable = true
          }, 350)
        },
        function (error) {
          console.log("Не получены журналы :(", error)
        }
      )
    }

    $scope.Login = function (LoginForm) {
      $scope.dataLoading = true
      AuthenticationService.Login(LoginForm.login.$modelValue, LoginForm.password.$modelValue, function (response) {
        if (response.success) {
          AuthenticationService.SetCredentials(LoginForm.login.$modelValue, LoginForm.password.$modelValue)
          $scope.InitJournals() // инициализация страницы с журналами
          $scope.dataLoading = false
        } else {
          $scope.error = response.message
          $scope.dataLoading = false
        }
      })
    }

    // GET все журналы и далее CRUD с каждым журналом
    $scope.journalsurl = BASE_URL + "backend_minecite/api/connector/v1/journal"

    $scope.folder = FOLDER
    //$scope.folder = "/";

    $scope.journalsserverResource = $resource(
      $scope.journalsurl,
      {},
      {
        updateRecommended: { method: "PUT", params: {} }
      }
    )

    // Если пользователь авторизовался
    if ($rootScope.globals.currentUser) {
      $scope.InitJournals()
    }

    $scope.changeRecommended = function (index) {
      $scope.journalsserverResource
        .updateRecommended({
          issn: $scope.journals[index].issn,
          is_recommended: $scope.journals[index].is_recommended
        })
        .$promise.then(
          function (response) {},
          function (error) {}
        )
    }
  }
])

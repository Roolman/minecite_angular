mineciteApp.controller("mineciteCtrl", function ($rootScope, $scope, $resource, $timeout, $window, makepdfService, $sce, $http) {
  // baseurl
  let baseurl = BASE_URL + "backend_minecite/api"
  let searchurl = baseurl + "/public/v1/search"
  let subjecturl = baseurl + "/connector/v1/subject"

  $scope.articlesLoading = false

  $scope.folder = FOLDER
  $scope.currentMainview = "answers.html"

  // Параметры поискового запроса
  $scope.searchParams = {
    text: "", // Текст запроса
    search_types: [
      // Где искать
      { value: "authors", checked: true },
      { value: "title", checked: true },
      { value: "abstract",checked: true },
      { value: "keywords", checked: true },
      { value: "ref", checked: true },
      { value: "doi", checked: true },
      { value: "issn", checked: true }
    ],
    golden_access: false, // Искать в закрытых источниках
    exact: false, // Точное совпадение
    recommended_journals_only: false,
    recommended_subjects_only: false,
    citation_sources: [
      // Искать в базах цитирования
      { value: "Scopus", name: "Scopus", active: true },
      { value: "RINZ", name: "РИНЦ", active: false },
      { value: "Web of science", name: "Web of science", active: false }
    ],
    journal_categories: [], // Категории журналов заполняются через запрос к серверу
    journal_categories_settings: {
      // displayProp: "title",
      template: "{{option}}",
      enableSearch: true,
      scrollable: true,
      buttonClasses: "btn btn-outline waves-effect wide selectpicker",
      // idProperty: "id",
      smartButtonMaxItems: 1,
      scrollableHeight: "400",
      smartButtonTextConverter: function (itemText, originalItem) {
        return $scope.searchParams.selected_categories.length + "  " +$rootScope.localeText.selected
      }
    }, // searchField: 'age',
    slider_years: {
      // Параметры слайдера поиска по годам
      minValue: 1972,
      maxValue: new Date().getFullYear() + 1,
      options: {
        floor: 1972,
        ceil: new Date().getFullYear() + 1
        //showTicksValues: 4 !!! Промежуток должен делиться на это число
      }
    },
    slider_quart: {
      // Параметры слайдера поиска по квартилям
      minValue: "Без квартиля",
      maxValue: "Q1",
      options: {
        showTicks: true,
        stepsArray: [
          { value: "Без квартиля", legend: "" },
          { value: "Q4", legend: "Q4" },
          { value: "Q3", legend: "Q3" },
          { value: "Q2", legend: "Q2" },
          { value: "Q1", legend: "Q1" }
        ]
      }
    },
    currentPage: 1, // Текущая страница
    itemsperPage: [{ value: 5 }, { value: 10 }, { value: 20 }],
    selected_itemsperPage: {},
    selected_sources: [],
    selected_categories: [],
    getTitles: function () {
      // Получаем из списка объектов выбранных категорий - список только названий
      var titles_list = []
      for (var i = 0, len = this.selected_categories.length; i < len; i++) {
        titles_list.push(this.selected_categories[i].title)
      }
      return titles_list
    },
    mindate: function () {
      return this.slider_years.minValue //  + "-01-01"
    },
    maxdate: function () {
      return this.slider_years.maxValue //  + "-12-31"
    },
    getQuartile: function (type) {
      var value = this.slider_quart.minValue
      if (type == "min") {
        value = this.slider_quart.minValue
      }
      if (type == "max") {
        value = this.slider_quart.maxValue
      }
      if (value == "Без квартиля") {
        return 5
      } else {
        return parseInt(value.split("")[1])
      }
    },
    getsearchTypes: function () {
      var search_types = []
      for (var i = 0, len = this.search_types.length; i < len; i++) {
        if (this.search_types[i].checked) {
          search_types.push(this.search_types[i].value)
        }
      }
      return search_types
    },
    getCategories: function () {
      var categories = []
      this.selected_categories.forEach((cat) => {
        JOURNALS_CAT[cat].forEach((x) => {
          if (BACKEND_CAT.includes(x)) {
            categories.push(x)
          }
        })
      })
      return categories
    },
    submitted: false
  }
  // NOTE: Так, потому что localeText изменяется (смена языка)
  $scope.getLocaleSearchTypeName = (value) => {
    switch(value){
      case "authors" :
        return $rootScope.localeText.searchInAuthors
      case "title" :
        return $rootScope.localeText.searchInTitles
      case "abstract" :
        return $rootScope.localeText.searchInAnnotations
      case "keywords" :
        return $rootScope.localeText.searchInKeywords
      case "ref" :
        return $rootScope.localeText.searchInLink
      case "doi" :
        return "DOI"
      case "issn" :
        return "ISSN"
      default:
        return ""
    }
  }

  //Получаем категории и суем в список поисковых параметров
  $scope.subjectsresource = $resource(subjecturl, {}, {})
  $scope.subjectsresource.get().$promise.then(
    function (res) {
      // Получаем категории из ответа res.subjects
      // Временно берем из константы !!!
      const categories = Object.keys(JOURNALS_CAT)
      $scope.searchParams.journal_categories = categories
      // Делаем селект сразу всех категорий
      // Обновляем селект с новыми данными
      $timeout(function () {
        $(".selectpicker").selectpicker("refresh")
      }, 1)
    },
    function (error) {
      // При ошибке надо подумать что делать !!!
    }
  )

  $scope.searchParams.selected_sources = [{ value: "Scopus", name: "Scopus", active: true }]

  $scope.articles = {
    data: [],
    total: 0
  }

  $scope.articlesResource = $resource(
    searchurl,
    {},
    {
      getarticles: {
        method: "POST",
        isArray: false,
        params: {
          query: "DEM"
        },
        headers: { "Content-Type": "application/json; charset=UTF-8" }
      }
    }
  )

  var search_params = {}

  $scope.search = function (searchform) {
    if (!searchform || searchform.$valid) {
      if (searchform) {
        makepdfService.clearItemsInPDF()
        // Если запрос отправлен из формы, то обновляем параметры поискового запроса
        search_params = {
          query: $scope.searchParams.text, // строка поискового запроса
          // exact: $scope.searchParams.exact, // точное соответствие запросу
          // is_open: $scope.searchParams.golden_access, // если True - то искать только в открытых источниках
          // recommended_journals_only:
          //   $scope.searchParams.recommended_journals_only,
          // recommended_subjects_only:
          //   $scope.searchParams.recommended_subjects_only,
          date_start: $scope.searchParams.mindate(), // Начальная дата
          date_end: $scope.searchParams.maxdate(), // Конечное
          // subject: $scope.searchParams.getTitles(), // Выбранные категории
          subjects: $scope.searchParams.getCategories(),
          fields: $scope.searchParams.getsearchTypes(), // Где искать
          // quartile_min: $scope.searchParams.getQuartile("max"), // наоборот
          // quartile_max: $scope.searchParams.getQuartile("min"),
          // order: "date",
          page_number: $scope.searchParams.currentPage - 1,
          rows: $scope.searchParams.selected_itemsperPage.value
        }
      } else {
        search_params.page_number = $scope.searchParams.currentPage - 1
        search_params.rows = $scope.searchParams.selected_itemsperPage.value
      }
      $scope.articlesLoading = true
      $http({
        method: "POST",
        data: JSON.stringify(search_params),
        url: searchurl,
        headers: { "Content-Type": "application/json; charset=UTF-8" }
      }).then(
        function (res) {
          $scope.currentMainview = "answers.html"
          // Получив корректный ответ заполняем данными объект
          $scope.articles.data = res.data.articles
          $scope.articles.total = res.data.total
          $scope.searchParams.submitted = false
          $scope.articlesLoading = false
        },
        function (error) {
          $scope.articles.total = 0
          $scope.searchParams.submitted = false
          $scope.articlesLoading = false
          if (error.status == 404) {
            $scope.currentMainview = "error404.html"
          } else {
            $scope.currentMainview = "error.html"
          }
        }
      )
    } else {
      $timeout(function () {
        $scope.searchParams.submitted = false
      }, 2000)
      // Здесь наверное просто ничего не писать - форма и так предупредит заполнить данные
    }
  }

  $scope.convertText = function (text) {
    //console.log(encodeURIComponent('\xa9'))
    //console.log(encodeURIComponent(text.substring(0, 4)))
    //return $sce.trustAsHtml(decodeURIComponent(encodeURIComponent(smth)))
    //console.log(text.replace('\xa9','KEK'))
    return $sce.trustAsHtml(decodeURIComponent(encodeURIComponent(text)))
    //return text
  }

  $scope.gotoScopusurl = function (eid) {
    $window.open("https://www.scopus.com/inward/record.uri?partnerID=HzOxMe3b&scp=" + eid.substr(7) + "&origin=inward")
  }
  $scope.gotoJournalurl = function (url) {
    $window.open(url)
  }

  $scope.checkParaminObject = function (param, obj) {
    if (param in obj) {
      return true
    } else {
      return false
    }
  }

  // Проверить находится ли элемент в списке на экспорт пдф
  $scope.checkIteminList = function (id) {
    return makepdfService.checkIteminList(id)
  }
  // Очистить данные в пдф
  $scope.clearPdf = function () {
    makepdfService.clearItemsInPDF()
  }
  // Добавить элемент в список на экспорт пдф или убрать
  $scope.addItemToPDF = function (id) {
    var article = $scope.articles.data[id - ($scope.searchParams.currentPage - 1) * $scope.searchParams.selected_itemsperPage.value]
    makepdfService.addItemToPDF(article, id)
  }
  // Сгенерировать пдф
  $scope.getPDF = function () {
    makepdfService.getPDF()
  }

  window.onscroll = function () {
    $scope.scrollFunction()
  }

  $scope.scrollFunction = function () {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      document.getElementById("myBtn").style.display = "block"
    } else {
      document.getElementById("myBtn").style.display = "none"
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  $scope.topFunction = function () {
    document.body.scrollTop = 0 // For Safari
    document.documentElement.scrollTop = 0 // For Chrome, Firefox, IE and Opera
  }

  $scope.dropdownLanguageUpdated = true
  $scope.changeLanguage = function (){
    const setLocale = (title) => {
      switch(title){
        case "Рус":
          $rootScope.locale = "ru"
          break
        case "Eng":
          $rootScope.locale = "en"
          break
        default:
          $rootScope.locale = "ru"
      }

     $rootScope.localeText = $rootScope.getLocaleText()
     $rootScope.$apply()
     $scope.dropdownLanguageUpdated = false
     $rootScope.$apply()
     $timeout(() => {
      $scope.dropdownLanguageUpdated = true
      $rootScope.$apply()
    },20)
    }

    let data = document.querySelector("[data-id='lang-select']")
    // 10 мс, чтобы значение успело поменяться в DOM
    setTimeout( () => setLocale(data.title), 20)
  }

  $scope.citedArticleTitle = ""
  $scope.copied = ""
  $scope.cited = false

  $scope.citeAs = function(article){
    $scope.citedArticleTitle = article.title
    $scope.cited = true
    let prevCiteAlt = $rootScope.localeText.citeAlt

    let text = $scope.getCiteAsText(article)

    navigator.clipboard.writeText(text).then(function() {
      const copied = $rootScope.locale == "ru" ? "Скопировано " : "Copied "
      $scope.copied = copied
      $rootScope.localeText.citeAlt = copied + text

      $rootScope.$apply()

      setTimeout(() => {
        $rootScope.localeText.citeAlt = prevCiteAlt
        $scope.citedArticleTitle = ""
        $scope.cited = false
        $rootScope.$apply()
      }, 1000)

    }, function(err) {
    })
  }
  
  $scope.getCiteAsText = function(article){
    let authors = article.authors.split(`;`)
    let authorsNames = authors.reduce((ac,cur,i,_) => {
      let res = ""
      i == 1 ? ac += ", " : ""
      i < 2 && i < authors.length - 1 ? cur += ", " : ""
      i < authors.length && i < 3 ? res = ac + cur : res = ac
      return res
    })
    let etAl = authors.length > 3 ? ", et al" : ""
    let year = article.cover_date.slice(0,4)
    let issue = article.issue ? ` issue ${article.issue},` : ""
    let volume = article.volume ? ` vol. ${article.volume},` : ""
    let pages = article.pageRange ? ` pp. ${article.pageRange}.` : ""
    let doi = article.doi ? ` DOI: ${article.doi}` : ""
    let journalTitle = article.journal ? ` ${article.journal.title},` : ''

    let text = `${authorsNames}${etAl}. (${year}) ${article.title}.${journalTitle}${issue}${volume}${pages}${doi}`
    
    return text
  }
})

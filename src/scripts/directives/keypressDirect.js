mineciteApp.directive('onShiftTab', function() {
    return function(scope, element, attrs) {
        var map = {9: false, 16: false}
    
        element.on("keydown", function(event) {
            if (event.which in map) {
                map[event.which] = true
                if (map[9] && map[16]) {
                    scope.$apply(function(){
                        scope.$eval(attrs.onShiftTab, {'$event': event})
                    })
                    event.preventDefault()
                }
            }
        })
        element.on("keyup", function(event) {
            if (event.which in map) {
                map[event.keyCode] = false
            }
        })
    }
})
var mineciteApp = angular.module("mineciteApp", [
  "ngResource",
  "ngRoute",
  "ngCookies",
  "datatables",
  "rzSlider",
  "ui.bootstrap",
  "angularjs-dropdown-multiselect",
])

mineciteApp.config(function (
  $sceDelegateProvider,
  $routeProvider,
  $locationProvider
) {
  var mainPath = FOLDER + "views/main.html"
  var adminPath = FOLDER + "views/journals.html"

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    "**",
  ])
  // minecite/
  $routeProvider
    .when("/minecite", {
      templateUrl: mainPath,
      controller: "mineciteCtrl",
    })
    .when("/minecite/journals", {
      templateUrl: adminPath,
      controller: "adminCtrl",
    })
    .otherwise("/minecite")

  $locationProvider.html5Mode(true).hashPrefix("!")
})

mineciteApp.run([
  "$rootScope",
  "$location",
  "$cookieStore",
  "$http",
  "AuthenticationService",
  function ($rootScope, $location, $cookieStore, $http, AuthenticationService) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get("globals") || {}
    if ($rootScope.globals.currentUser) {
      $http.defaults.headers.common["Authorization"] =
        "Basic " + $rootScope.globals.currentUser.authdata // jshint ignore:line
    }

    $rootScope.$on("$locationChangeStart", function (event, next, current) {
      // redirect to login page if not logged in
      if (
        $location.path() == "minecite/journals" &&
        !$rootScope.globals.currentUser
      ) {
        //$location.path('/login')
      }
    })

    $rootScope.prevStep = function () {
      AuthenticationService.ClearCredentials()
    }

    $rootScope.locale = "ru"
    $rootScope.getLocaleText = function () {
      switch($rootScope.locale){
        case "ru":
          return {
            title: "Система цитирований MineCite",
            searchInputPlaceholder: "Введите запрос на английском языке (название, фамилию или ключевые слова)",
            pubYears: "Годы публикации",
            journalCat: "Категории журналов",
            selectJournalCat: "Выбрать категории журналов",
            selectJournalCheckAll: "Выбрать все",
            selectJournalUncheckAll: "Убрать все",
            selectJournalSearchPlaceholder: "Поиск...",
            selected: "Выбрано",
            searchPublications: "Поиск",
            searchIn: 'Искать в',
            searchInAuthors: 'Авторах',
            searchInTitles: 'Заголовках',
            searchInLink: 'Ссылках',
            searchInKeywords: 'Ключевых словах',
            searchInAnnotations: 'Аннотациях',
            searchError: 'Кажется, что-то пошло не так.\nПопробуйте еще раз или зайдите позже',
            searchNotFound: 'Похоже, статей с таким запросом нет.\nПопробуйте выполнить запрос с другими параметрами.',
            found: "Найдено",
            articles: "публикаций",
            showBy: "Показывать по",
            articlesByPage: "публикаций на странице",
            keywords: "Ключевые слова",
            categories: "Категории",
            pubDate: "Дата публикации",
            journal: "Журнал",
            authors: "Авторы",
            typeOfArticle: "Тип публикации",
            noQuartile: "Без квартиля",
            annotation: "Аннотация",
            readIn: "Почитать в",
            readArticle: "Почитать статью",
            citations: "цитирований",
            openaccess: "Статья в открытом доступе",
            citeAs: "Процитировать",
            citeAlt: "Скопировать цитату",
            citeAsArticle: "Цитировать как"
          }
        case "en":
          return {
            title: "MineCite",
            searchInputPlaceholder: "Type here (title, author or keywords)",
            pubYears: "Publications date range",
            journalCat: "Journal categories",
            selectJournalCat: "Select journal categories",
            selectJournalCheckAll: "Select all",
            selectJournalUncheckAll: "Deselect all",
            selectJournalSearchPlaceholder: "Search...",
            selected: "Selected",
            searchPublications: "Search",
            searchIn: 'Search in',
            searchInAuthors: 'Authors',
            searchInTitles: 'Titles',
            searchInLink: 'Links',
            searchInKeywords: 'Keywords',
            searchInAnnotations: 'Abstract',
            searchError: 'Error.\nPlease, try again.',
            searchNotFound: 'Publications not found.\nTry another search query.',
            found: "Found",
            articles: "publications",
            showBy: "Show by",
            articlesByPage: "publications per page",
            keywords: "Keywords",
            categories: "Categories",
            pubDate: "Publication date",
            journal: "Journal",
            authors: "Authors",
            typeOfArticle: "Publications type",
            noQuartile: "No quartile",
            annotation: "Abstract",
            readIn: "Watch in",
            readArticle: "Read publication",
            citations: "citations",
            openaccess: "Open access",
            citeAs: "Cite",
            citeAlt: "Copy citation to clipboard",
            citeAsArticle: "Cite as"
          }
      }
    }
    $rootScope.localeText = $rootScope.getLocaleText()
  },
])
